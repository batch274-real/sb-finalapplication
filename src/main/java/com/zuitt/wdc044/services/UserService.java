package com.zuitt.wdc044.services;

import com.zuitt.wdc044.models.User;

import java.util.Optional;

public interface UserService {
    // register user
    void createUser(User user);

    // Check username functionality
    Optional<User> findByUsername(String username);

}
